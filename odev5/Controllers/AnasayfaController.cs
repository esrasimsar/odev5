﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace odev5.Controllers
{
    public class AnasayfaController : Controller
    {
        // GET: Home
        public ActionResult Anasayfa()
        {

            String dil = Request.QueryString["Language"];

            if (@dil != null)
            {

                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(@dil);
                Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(@dil);

                HttpCookie cookie = new HttpCookie("Language");
                cookie.Value = @dil;
                Response.Cookies.Add(cookie);

            }

            return View();
        }
        // GET: Home
        public ActionResult Anneme()
        {
            return View();
        }
        // GET: Home
        public ActionResult Cocuguma()
        {
            return View();
        }
        // GET: Home
        public ActionResult Aktiviteler()
        {
            return View();
        }
        public ActionResult Psikologlar()
        {
            return View();
        }
    }
}