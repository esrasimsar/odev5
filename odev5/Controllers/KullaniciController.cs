﻿using odev5.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace odev5.Controllers
{
    public class KullaniciController : Controller
    {
        odevdb5Entities1 db = new odevdb5Entities1();

        // GET: Kullanici
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Giris()
        {
            return View();
        }
        public ActionResult Yeni()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Yeni([Bind(Include = "ad_soyad,email,sifre")] kullanicilar kullanici)
        {
            db.kullanicilars.Add(kullanici);
            db.SaveChanges();

            return View();
        }

    }
}