﻿using odev5.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace odev5.Controllers
{
    public class YonetimController : Controller
    {

        odevdb5Entities1 db = new odevdb5Entities1();

        // GET: Yonetim
        public ActionResult Anasayfa()
        {
            return View();
        }
        public ActionResult Kullanicilar()
        {
            var model = db.kullanicilars.ToList();

            return View(model);
        }
        public ActionResult KullaniciDuzenle()
        {
            return View();
        }
        public ActionResult KullaniciSil( int id = 0)
        {

            kullanicilar sil = db.kullanicilars.Find(id);
            db.kullanicilars.Remove(sil);
            db.SaveChanges();

            return RedirectToAction("Kullanicilar");
        }
    }
}