﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(odev5.Startup))]
namespace odev5
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
